import { Container } from '@mui/material';
import { Routes, Route } from 'react-router-dom';
import NewNote from './modules/notes/screens/NewNote';
import Notes from './modules/notes/screens/Notes';
import Header from './shared/components/Header';
import { useNavigate } from 'react-router-dom';

const App = () => {
  const navigate = useNavigate();

  function addNoteHandler() {
    navigate('/add');
  }
  return (
    <Container maxWidth="lg">
      <Header title="Notes" addNoteHandler={addNoteHandler} />
      <main>
        <Routes>
          <Route path="/" element={<Notes />} />
          <Route path="/add" element={<NewNote />} />
        </Routes>
      </main>
    </Container>
  );
};

export default App;
