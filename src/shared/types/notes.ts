export interface Note {
  id: number;
  date: Date;
  title: string;
  author: string;
  description?: string;
}
