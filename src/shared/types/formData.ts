export interface formData {
  date: Date;
  title: string;
  firstname: string;
  surname: string;
  description?: string;
}
