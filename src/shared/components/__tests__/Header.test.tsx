import { render, screen } from '@testing-library/react';
import Header from '../Header';

const testTitle = 'test title';

test('renders header title', () => {
  render(<Header title={testTitle} />);
  const titleElement = screen.getByText(testTitle);
  expect(titleElement).toBeInTheDocument();
});

test('renders header button', () => {
  render(<Header title={testTitle} />);
  const buttonElement = screen.getByText('Add note');
  expect(buttonElement).toBeInTheDocument();
});
