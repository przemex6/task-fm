import * as React from 'react';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Button } from '@mui/material';

interface Props {
  title: string;
  addNoteHandler: () => void;
}

const Header: React.FC<Props> = ({ title, addNoteHandler }) => {
  return (
    <>
      <Toolbar sx={{ borderBottom: 1, borderColor: 'divider', mb: 4 }}>
        <Typography
          component="h2"
          variant="h5"
          color="inherit"
          align="center"
          noWrap
          sx={{ flex: 1 }}>
          {title}
        </Typography>
        <Button variant="outlined" size="small" onClick={addNoteHandler}>
          Add note
        </Button>
      </Toolbar>
    </>
  );
};

export default Header;
