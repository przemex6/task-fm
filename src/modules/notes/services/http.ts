import http from '../../../shared/services/http';
import { Note } from '../../../shared/types/notes';

export enum NotesQueryKeys {
  GET_NOTES = 'GET_NOTES',
}

export const getNotes = () => http.get<Array<Note>>(`notes`);
export const sendNotes = (data: Omit<Note, 'id'>) =>
  http.post<Note>(`notes`, data);
