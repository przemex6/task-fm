import { CircularProgress, Container, Typography } from '@mui/material';
import { useMutation } from '@tanstack/react-query';
import NewNoteForm from '../components/NewNoteForm';
import { sendNotes } from '../services/http';
import { Note as NoteType } from '../../../shared/types/notes';
import { formData as formDataType } from '../../../shared/types/formData';

const NewNote = () => {
  const { isLoading, mutate } = useMutation<
    unknown,
    unknown,
    Omit<NoteType, 'id'>
  >(sendNotes);

  function onSubmit(data: formDataType) {
    const { firstname, surname, title, date, description } = data;
    const submitData: Omit<NoteType, 'id'> = {
      title,
      author: `${firstname} ${surname}`,
      date,
      description,
    };
    mutate(submitData, {
      onSuccess: async (data: unknown, context: unknown) => {
        alert('Success, your note added!');
      },
      onError: async (data: unknown, context: unknown) => {
        alert(`Fail, ${data}`);
      },
      onSettled: () => {
        //TODO
      },
    });
  }
  if (!isLoading)
    return (
      <Container component="main" maxWidth="xs">
        <Typography component="h2" variant="h3" align="center">
          Add New Note
        </Typography>
        <NewNoteForm onSubmit={onSubmit} />
      </Container>
    );

  return <CircularProgress />;
};

export default NewNote;
