import { CircularProgress, Grid } from '@mui/material';
import { useQuery } from '@tanstack/react-query';
import Note from '../components/Note';
import { getNotes, NotesQueryKeys } from '../services/http';

const Notes = () => {
  const { isLoading, data } = useQuery(
    [NotesQueryKeys.GET_NOTES],
    getNotes,
    {},
  );

  if (!isLoading && data) {
    return (
      <Grid container spacing={4}>
        {data.data.map(note => (
          <Note note={note} />
        ))}
      </Grid>
    );
  }

  return <CircularProgress />;
};

export default Notes;
