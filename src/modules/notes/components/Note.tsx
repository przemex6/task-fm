import { Card, CardContent, Grid, Typography } from '@mui/material';
import { FC } from 'react';
import { Note as NoteType } from '../../../shared/types/notes';

interface Props {
  note: NoteType;
}

const Note: FC<Props> = ({ note }) => {
  return (
    <Grid item xs={12} md={6} key={note.id}>
      <Card sx={{ display: 'flex' }}>
        <CardContent sx={{ flex: 1 }}>
          <Typography component="h2" variant="h5">
            {note.title} &bull; {note.author}
          </Typography>
          <Typography variant="subtitle1" color="text.secondary">
            {new Date(note.date).toDateString()}
          </Typography>
          <Typography variant="subtitle1" paragraph>
            {note.description}
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default Note;
