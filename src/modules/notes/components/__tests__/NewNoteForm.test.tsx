import { render, screen } from '@testing-library/react';
import NewNoteForm from '../NewNoteForm';

test('renders form Inputs', () => {
  render(<NewNoteForm onSubmit={() => {}} />);
  expect(screen.getByRole('textbox', { name: /surname/i })).toBeInTheDocument();
  expect(
    screen.getByRole('textbox', { name: /firstname/i }),
  ).toBeInTheDocument();
  expect(screen.getByRole('textbox', { name: /title/i })).toBeInTheDocument();
  expect(
    screen.getByRole('textbox', { name: /description/i }),
  ).toBeInTheDocument();
});

test('renders form buttons', () => {
  render(<NewNoteForm onSubmit={() => {}} />);
  expect(screen.getByRole('button', { name: /Submit/i })).toBeInTheDocument();
  expect(screen.getByRole('button', { name: /Reset/i })).toBeInTheDocument();
});
