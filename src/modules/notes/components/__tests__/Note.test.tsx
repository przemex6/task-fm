import { render, screen } from '@testing-library/react';
import Note from '../Note';

const testDate = '2022-10-16 20:44:04.528472+02';

const testNote = {
  id: 1,
  date: new Date(testDate),
  title: 'test title',
  author: 'John Doe',
  description: 'test description',
};

test('renders note date', () => {
  render(<Note note={testNote} />);
  const dateElement = screen.getByText(new Date(testDate).toDateString());
  expect(dateElement).toBeInTheDocument();
});

test('renders note title', () => {
  render(<Note note={testNote} />);
  const titleElement = screen.getByText(testNote.title, { exact: false });
  expect(titleElement).toBeInTheDocument();
});

test('renders note author', () => {
  render(<Note note={testNote} />);
  const authorElement = screen.getByText(testNote.author, { exact: false });
  expect(authorElement).toBeInTheDocument();
});

test('renders note description', () => {
  render(<Note note={testNote} />);
  const descriptionElement = screen.getByText(testNote.description);
  expect(descriptionElement).toBeInTheDocument();
});
