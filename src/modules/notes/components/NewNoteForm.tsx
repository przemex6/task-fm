import { TextField } from '@mui/material';
import React, { FC } from 'react';
import { Button } from '@mui/material';
import { useForm, Controller } from 'react-hook-form';
import { formData as formDataType } from '../../../shared/types/formData';

interface Props {
  onSubmit: (data: formDataType) => unknown;
}

const NewNoteForm: FC<Props> = ({ onSubmit }) => {
  const {
    handleSubmit,
    reset,
    control,
    formState: { errors },
  } = useForm<formDataType>();

  return (
    <form>
      <Controller
        control={control}
        name="firstname"
        defaultValue=""
        rules={{ required: 'Field Required' }}
        render={({ field }) => (
          <TextField
            {...field}
            label="Firstname"
            style={{ margin: '15px 0' }}
            required
            error={!!errors?.firstname?.message}
          />
        )}
      />
      <Controller
        control={control}
        name="surname"
        defaultValue=""
        rules={{ required: 'Field Required' }}
        render={({ field }) => (
          <TextField
            {...field}
            label="Surname"
            style={{ margin: '15px 0' }}
            error={!!errors?.surname?.message}
          />
        )}
      />

      <Controller
        control={control}
        name="title"
        defaultValue=""
        rules={{ required: 'Field Required' }}
        render={({ field }) => (
          <TextField
            {...field}
            fullWidth
            label="Title"
            style={{ margin: '15px 0' }}
            error={!!errors?.title?.message}
          />
        )}
      />
      <Controller
        control={control}
        name="date"
        rules={{ required: 'Field Required' }}
        render={({ field }) => (
          <TextField
            {...field}
            fullWidth
            label="Date"
            type="datetime-local"
            style={{ margin: '15px 0' }}
            error={!!errors?.date?.message}
          />
        )}
      />

      <Controller
        control={control}
        name="description"
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            fullWidth
            multiline
            rows={4}
            label="Description"
            style={{ margin: '15px 0' }}
          />
        )}
      />

      <Button
        onClick={handleSubmit(onSubmit)}
        variant={'outlined'}
        style={{ margin: '10px 5px' }}>
        Submit
      </Button>
      <Button
        onClick={() => reset()}
        variant={'outlined'}
        style={{ margin: '10px 5px' }}>
        Reset
      </Button>
    </form>
  );
};

export default NewNoteForm;
