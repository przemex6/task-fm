# ReactJS Recruitment Task

> Read through the task. If you already have sample code that covers the topics of the task that represent your skill level, you can use that instead. Bare in mind, that if we decide it's not sufficient, we can ask you to do the task regardless ;)

## Environment

App is based on CreateReactApp with Typescript and uses JSON Server as API mock. We use prettier for code formatting. Make sure to create `.env` file that is based on `.env.example`.

### `npm start`

Starts app in dev mode.

### `npm tests`

Runs tests.

### `npm run server`

Starts JSON Server.

## Your task

Add new page (available using "Add Note" button in header) which has form that enables user to add new note.

Requirements:
* fields `author`, `title` and `date` are required,
* field `author` should be combined from two inputs - name and surname,
* user should be able to set date manually,
* add tests to components, similar to tests that are already in repository

Add missing functionalities to existing app. You can add new libraries if you want to.
